<?php

// +----------------------------------------------------------------------

// | OneThink [ WE CAN DO IT JUST THINK IT ]

// +----------------------------------------------------------------------

// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.

// +----------------------------------------------------------------------

// | Author: 廖春贵 <liaodeity@foxmail.com> <http://www.iooen.com>

// +----------------------------------------------------------------------



namespace Home\Controller;

use Think\Controller;

/**

 * 清理缓存控制器

 */

class ClearController extends HomeController{

	public function index(){

		$dirname = './Runtime/';



		//清文件缓存

		$dirs	=	array($dirname);



		//清理缓存

		foreach($dirs as $value) {

			$this->rmdirr($value);

			echo "<div style='border:2px solid green; background:#f1f1f1; padding:20px;margin:20px;width:800px;font-weight:bold;color:green;text-align:center;'>\"".$value."\" have been cleaned clear! </div> <br /><a href=".U('/').">返回首页</a><br />";

		}



		@mkdir($dirname,0777,true);



	}

	public function rmdirr($dirname) {

		if (!file_exists($dirname)) {

			return false;

		}

		if (is_file($dirname) || is_link($dirname)) {

			return unlink($dirname);

		}

		$dir = dir($dirname);

		if($dir){

			while (false !== $entry = $dir->read()) {

				if ($entry == '.' || $entry == '..') {

					continue;

				}

				$this->rmdirr($dirname . DIRECTORY_SEPARATOR . $entry);

			}

		}

		$dir->close();

		return rmdir($dirname);

	}

	public function U(){

		return false;

	}

}