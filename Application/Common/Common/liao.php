<?php 
// +----------------------------------------------------------------------
// | JianBaiZhan [ YOU LIKE I LIKE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2113 http://www.jianbaizhan.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: 廖春贵 <liaodeity@foxmail.com> <http://www.jianbaizhan.com>
// +----------------------------------------------------------------------

/**
 * [p 测试打印]
 * @param  [type] $arr [数据或字符]
 */
function p($arr){
	if(is_array($arr)){
		echo '<pre>';
		print_r($arr);
		echo '</pre>';
	}else{
		var_dump($arr);
	}
}
