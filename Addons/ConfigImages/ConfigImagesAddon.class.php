<?php

namespace Addons\ConfigImages;
use Common\Controller\Addon;

/**
 * 图片批量上传插件
 * @author 原作者:tjr&jj
 * @author 木梁大囧
 */

    class ConfigImagesAddon extends Addon{
        public $info = array(
            'name' => 'ConfigImages',
            'title' => '配置多图上传',
            'description' => '支持多图上传',
            'status' => 1,
            'author' => 'tjr&jj',
            'version' => '1.2'
        );

        public function install(){
            return true;
        }

        public function uninstall(){
            return true;
        }

        //实现的ConfigImages钩子方法
        public function ConfigImages($param){
            $name = $param['name'] ?: 'pics';
            $valArr = $param['value'] ? explode(',', $param['value']) : array();
            $this->assign('name',$name);
            $this->assign('valStr',$param['value']);
            $this->assign('valArr',$valArr);
            $this->display('upload');
        }
    }